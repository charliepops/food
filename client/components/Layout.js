import React, { Component } from 'react';
import Header from './_common/Header';
import ProfileNav from './_common/ProfileNav';
import Footer from './_common/Footer';

class Layout extends Component {

  render() {
    return (
      <div>
        <ProfileNav/>
        <Header/>
        {this.props.children}
        <Footer/>
      </div>
    );
  }

}

Layout.propTypes = {
  children: React.PropTypes.element
};

export default Layout;
