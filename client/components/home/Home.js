import React, { Component } from 'react';
import Testimonials from '../_common/Testimonials';
import HeroPrimary from '../_common/HeroPrimary';
import FeatureBlade from '../_common/FeatureBlade';
import FeaturesSteps from '../_common/FeaturesSteps';
class Home extends Component {

  render() {
    return (
      <main role="main">
        <HeroPrimary />
        <FeaturesSteps />
        <FeatureBlade />
        <Testimonials />
      </main>
    );
  }

}

export default Home;
