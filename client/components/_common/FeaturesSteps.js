import React, { Component } from 'react';


class HeroPrimary extends Component {


  render() {


    return (
      <section className="c-slab c-slab-gray-15 c-features u-text-center">
        <div className="container">
          <h2 className="c-text-secondary sp-bottom-3">COMO FUNCIONA</h2>
          <div className="row">
            <div className="col-xs-12 col-sm-6 col-md-3">
              <div className="c-feature__item">
                <img className="c-feature__media" src="/icon-calendar-red.png" />
              </div>
              <h3 className="c-text-quaternary sp-bottom-2">
                <span className="c-bubble c-bubble--small c-bubble--secondary-dark">1</span>
                <span className="u-ui-vertical-center">Escoge el plan</span>
              </h3>
              <p>Puedes determinar cual plan se adapta a sus metas o necesidades.</p>
            </div>
            <div className="col-xs-12 col-sm-6 col-md-3">
              <div className="c-feature__item">
                <img className="c-feature__media" src="../assets/images/icon-checklist-red.png" />
              </div>
              <h3 className="c-text-quaternary sp-bottom-2">
                <span className="c-bubble c-bubble--small c-bubble--secondary-dark">2</span>
                <span className="u-ui-vertical-center">Personaliza</span>
              </h3>
              <p>Tomando en cuenta restricciones alimentarias, preferencias, alergias, intolerancias</p>
            </div>
            <div className="col-xs-12 col-sm-6 col-md-3">
              <div className="c-feature__item">
                <img className="c-feature__media" src="../assets/images/icon-truck-red.png" />
              </div>
              <h3 className="c-text-quaternary sp-bottom-2">
                <span className="c-bubble c-bubble--small c-bubble--secondary-dark">3</span>
                <span className="u-ui-vertical-center">Entrega</span>
              </h3>
              <p>En la plataforma escoge el lugar de entrega para dejar las comidas.</p>
            </div>
            <div className="col-xs-12 col-sm-6 col-md-3">
              <div className="c-feature__item">
                <img className="c-feature__media" src="../assets/images/icon-plate-red.png" />
              </div>
              <h3 className="c-text-quaternary sp-bottom-2">
                <span className="c-bubble c-bubble--small c-bubble--secondary-dark">4</span>
                <span className="u-ui-vertical-center">Distruta</span>
              </h3>
              <p>Calienta en el microondas y disfruta una comida saludable y deliciosa.</p>
            </div>
          </div>
        </div>
      </section>
    );
  }

}

export default HeroPrimary;
