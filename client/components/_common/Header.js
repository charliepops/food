import React, { Component } from 'react';
import {Link, IndexLink} from 'react-router';
import classNames from 'classnames';

class Header extends Component {

  constructor(props) {
    super(props);
    this.state = {
      active: false,
    };
  }

  onChangeMenu() {
    this.setState({
      active: !this.state.active
    })
  }

  render() {
    const btnClass = classNames({
      'header__btn' : true,
      'header__btn--active': this.state.active,
      'u-hidden-desktop': true
    });

    return (
      <header className="header">
        <div className="container header__wrapper">
          <h1 className="header__logo"><IndexLink to="/">Logo</IndexLink></h1>
          <nav role="navigation" className="header__nav u-hidden-mobile">
            <ul className="nav-main">
              <li className="nav-main__item"><Link className="nav-main__link" to="/menu">Menu</Link></li>
              <li className="nav-main__item"><Link className="nav-main__link" to="/plans">Plans</Link></li>
              <li className="nav-main__item"><Link className="nav-main__link" to="/about">About Us</Link></li>
              <li className="nav-main__item"><Link className="nav-main__link" to="/contact">Contact Us</Link></li>
            </ul>
          </nav>
          <button type="button" className={btnClass} onClick={this.onChangeMenu.bind(this)}>
            <span className="line"></span>
          </button>
        </div>
      </header>
    );
  }

}


export default Header;
