import React, { Component } from 'react';


class HeroPrimary extends Component {


  render() {


    return (
      <section className="c-hero c-hero--primary">
        <div className="c-hero__wrap">
          <div className="container">
            <div className="row">
              <div className="col-sm-6">
                <div className="c-hero__text">
                  <h2 className="c-text-secondary c-color-white">COMIDA SALUDABLE <br/> EN TRES PASOS:</h2>
                  <p className="c-text-quaternary c-color-white">
                    <ol className="u-unstyled-list">
                      <li>1. Cocción al SOUS VIDE</li>
                      <li>2. Enfriamiento rápido (pasteurización)</li>
                      <li>3. Regeneración (calienta y disfruta)</li>
                    </ol>
                  </p>
                </div>
              </div>
              <div className="col-sm-6">
                <iframe src="https://player.vimeo.com/video/27243869" frameBorder="0" width="100%" height="280"></iframe>
              </div>
            </div>
          </div>
        </div>
      </section>
    );
  }

}

export default HeroPrimary;
