import React, { Component } from 'react';


class Footer extends Component {


  render() {


    return (
      <footer className="footer">
        <div className="container">
          <h2>Meal Prep</h2>
          <div className="row">
            <div className="col-xs-12 col-sm-4">Acerca de nosotros</div>
            <div className="col-xs-12 col-sm-4"></div>
            <div className="col-xs-12 col-sm-4"></div>
            <div className="col-xs-12 col-sm-4">Social</div>
          </div>
        </div>
        <p className="u-text-center">Derechos reservados</p>
      </footer>
    );
  }

}


export default Footer;
