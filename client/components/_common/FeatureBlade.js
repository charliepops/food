import React, { Component } from 'react';

import imgUrl from '../assets/images/food-7.jpg';

class FeatureBlade extends Component {


  render() {


    return (
      <section className="c-blade">
        <div className="container container--fluid">
          <div className="row row--table">
            <div className="col-xs-12 col-sm-5 col-middle">
              <h2 className="c-text-quaternary c-blade__text">Utilizamos la cocción al <strong>SOUS VIDE</strong> como único método de cocción que preserva todos los nutrientes en los alimentos.</h2>
            </div>
            <div className="col-xs-12 col-sm-7 col-middle c-blade__media">
              <img className="c-blade__image" src={imgUrl} alt="" />
            </div>
          </div>
        </div>
      </section>
    );
  }

}

export default FeatureBlade;
