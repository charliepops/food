import React, { Component } from 'react';


class Testimonials extends Component {


  render() {


    return (
      <section className="c-slab c-slab-gray-15">
        <div className="container container--large">
          <h2 className="c-text-secondary u-text-center">TESTIMONIOS</h2>
          <p className="u-text-center sp-bottom-4">Esto es lo que algunos clientes han dicho de nosotros:</p>
          <div className="c-quote">
            <div className="row">
              <div className="col-xs-12 col-sm-3 u-text-center">
                <img className="c-quote__media" src="../assets/images/avatar-testimonial-1.jpg" />
                <cite>‎Ana Patricia Araya</cite>
              </div>
              <div className="col-xs-12 col-sm-9">
                <div className="c-quote__text">
                  <blockquote>
                    <p>Hola. Tengo escaso un mes de haber contratado con VALEDI, pero debo apuntar que la experiencia ha sido realmente positiva. Trabajo - la mayoría del tiempo- desde mi casa y me estaba ocurriendo que redactando y estudiando terminé postergando mis horarios de comida y lo más grave: la calidad de los alimentos, sin olvidar que cuando cocinaba todo se me quemaba porque mi concentración estaba en otro lado Ahora, simplemente me levanto de mi oficina y caliento!! Disfruto comida saludable y de muy buena calidad y exquisito sabor. Realmente recomendado!!!!!!</p>
                  </blockquote>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    );
  }

}


export default Testimonials;
