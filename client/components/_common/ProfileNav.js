import React, { Component } from 'react';
// import classNames from 'classnames';

class ProfileNav extends Component {

  constructor(props) {
    super(props);
    this.state = {
      active: false,
    };
  }



  onChangeMenu() {
    this.setState({
      active: !this.state.active
    })
  }

  render() {
    // const { profile } = this.props;
    //
    // const dropdownMenu = classNames({
    //   'c-menu-dropdown': true,
    //   'c-menu-dropdown--active': this.state.active,
    //   'c-menu-dropdown--secondary-header ': true,
    //   'u-unstyled-list': true
    // })

    return (
      <section className="header-secondary">
        <div className="container">
          <div className="c-profile-nav u-pull-right">

              <div className="c-profile-nav__text">
                <ul className="c-profile-nav__list u-unstyled-list">
                  <li className="c-profile-nav__list-item">
                    <a href="#" className="c-profile-nav__link" >Sign in</a>
                  </li>
                  <li className="c-profile-nav__list-item">
                    <a href="#" className="btn btn--secondary btn--small">Sign up</a>
                  </li>
                </ul>
              </div>

          </div>
        </div>
      </section>
    );
  }

}

// ProfileNav.propTypes = {
//   dispatch: React.PropTypes.func,
//   profile: React.PropTypes.object
// };


export default ProfileNav;
