import './styles/index.scss';
import React from 'react';
import { render } from 'react-dom';
import App from './components/App'
import { AppContainer } from 'react-hot-loader'

const renderApp = () => {

  render(
    <AppContainer>
      <App/>
    </AppContainer>,
    document.getElementById('app')
  );

}

if (module.hot) {
  module.hot.accept('./components/App', () => {
    renderApp();
  });
}

renderApp();
