import React from 'react';
import { Route, IndexRoute } from 'react-router';
import Layout from './components/Layout';
import Home from './components/home/Home';
import About from './components/about/About';

export default (
  <Route path="/" component={Layout}>
    <IndexRoute component={Home} />
    <Route path="about" component={About}/>
  </Route>
);
