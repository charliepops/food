const path = require('path');
const webpack = require('webpack');
const merge = require('webpack-merge');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

const TARGET = process.env.npm_lifecycle_event;

const PATHS = {
  client: path.join(__dirname, 'client'),
  build: path.join(__dirname, 'build')
};

const common  = {
  entry: {
    app: [
      'react-hot-loader/patch',
      'webpack-hot-middleware/client?reload=false',
      path.join(PATHS.client, 'index.js')
    ],
    vendor: ['react', 'react-dom']
  },
  output: {
    path: PATHS.build,
    filename: 'app.js',
    publicPath: '/'
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        loader: 'eslint-loader',
        enforce: 'pre',
        exclude: /node_modules/,
        options: {
          configFile: '.eslintrc'
        }
      },
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: 'babel-loader?cacheDirectory',
        include: PATHS.client
      },
      {
        test: /\.(png|jpg|gif|svg|woff|woff2)$/,
        use: 'url-loader?limit=10000'
      },
      {
        test: /\.(mp4|ogg)$/,
        use: 'file-loader'
      }
    ]
  },
  plugins: [
    new webpack.optimize.CommonsChunkPlugin({
      name: 'vendor',
      minChunks: Infinity,
      filename: 'vendor.js',
    }),
  ]
}

var config;

switch (TARGET) {
  case 'build':
    config = merge(common, {
      module: {
        rules: [
          {
            test: /\.scss$/,
            use: ExtractTextPlugin.extract({
              fallback: 'style-loader',
              use: ['css-loader', 'sass-loader'],
            }),
            include: PATHS.client
          }
        ]
      },
      plugins: [
        new webpack.DefinePlugin({
          'process.env.NODE_ENV': JSON.stringify('production')
        }),
        new webpack.optimize.UglifyJsPlugin({
          sourceMap: true
        }),
        new ExtractTextPlugin('app.css'),
      ]
    });
    break;

  default:
    config = merge(common, {
      devtool: 'eval-source-map',
      module: {
        rules: [
          {
            test: /\.scss$/,
            use: ['style-loader', 'css-loader', 'sass-loader'],
            include: PATHS.client
          }
        ]
      },
      plugins: [
        new webpack.DefinePlugin({
          'process.env.NODE_ENV': JSON.stringify('development')
        }),
        new webpack.HotModuleReplacementPlugin()
      ],
      devServer: {
        historyApiFallback: true,
        hot: true,
        inline: true,
        stats: 'errors-only',
        port: 3000
      }
    });
}

module.exports = config;
