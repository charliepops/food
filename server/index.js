const express = require('express');
const webpack = require('webpack');
const path = require('path');
const app = express();

if (process.env.NODE_ENV === 'development') {

  const webpackConfig = require('../webpack.config');
  const compiler = webpack(webpackConfig);

  app.use(require('webpack-dev-middleware')(compiler, {
    noInfo: true,
    publicPath: webpackConfig.output.publicPath
  }));

  app.use(require('webpack-hot-middleware')(compiler));

}

app.use((req, res) => {
  res.set('Content-Type', 'text/html')
    .status(200)
    .end(
      `<!doctype html>
      <html>
        <head>
          <meta charset="utf-8">
          <title>Food</title>
        </head>
        <body>
          <div id="app"></div>
          <script src="vendor.js"></script>
          <script src="app.js"></script>
        </body>
      </html>`
    );
});

app.listen(3000, () => {
  console.log('server running...');
});
